#!/usr/bin/env python3

from Report import Report
rpt = Report()

## TODO: Add your code here
import csv
import time
import os
import sys
import array
import operator
start_time = time.time() # Calculate Time Lapsed
 
# Count FIPS for all businesses
high_total_annual_wages = [0,0]
fips_used = []
fips_estab = [0,0]
fips_empl = [0,0]

# Count FIPS for Software Publishing Business
sp_high_total_annual_wages = [0,0]
sp_fips_used = []
sp_fips_estab = [0,0]
sp_fips_empl = [0,0]

# Reset Report
rpt.all.count = 0
rpt.all.total_pay = 0
rpt.all.total_estab = 0
rpt.all.total_empl = 0
rpt.soft.count = 0
rpt.soft.total_pay = 0
rpt.soft.total_estab = 0
rpt.soft.total_empl = 0

# Unique/distinct Value Lists
unique_annual_wages = []
distinct_annual_wages = []
sp_unique_annual_wages = []
sp_distinct_annual_wages = []
unique_estab = []
distinct_estab = []
sp_unique_estab = []
sp_distinct_estab = []
unique_empl = []
distinct_empl = []
sp_unique_empl = []
sp_distinct_empl = []

# Top 5 Value Dictonarys
area_codes = {}
annual_wages = []
area_estab = []
average_employment = []
s_annual_wages = []
s_area_estab = []
s_average_employment = []

# Get File dir
single_file = None 
area_titles = None

# Read the "2017.annual.singlefile.csv"
try:
    single_file = os.getcwd() + "/" + str(sys.argv[1]) + "/2017.annual.singlefile.csv"
    try: 
        fh = open(single_file) 
    except FileNotFoundError: 
        print("\033[93mERROR: No valid data directory given. Please specify a directory containing a file called '2017_annual_singlefile.csv' \033[0m")
        print("File Directory Used: " +single_file)
        sys.exit(0)
    area_titles = os.getcwd() + "/" + str(sys.argv[1]) + "/area_titles.csv"
    try: 
        fh = open(area_titles) 
    except FileNotFoundError: 
        print(single_file)
        print("\033[93mERROR: No valid data directory given. Please specify a directory containing a file called 'area_titles.csv' \033[0m")
        sys.exit(0)
except:
    sys.exit(0)

# Assign area_codes dictonary with FIPS Code as Key and area title as Value
with open(area_titles) as csv_file:
    area_titles_cvs_reader = csv.reader(csv_file, delimiter=',')
    line_count = 0
    for row in area_titles_cvs_reader:
        area_codes[row[0]] = row[1]

# Open File
area_titles_cvs_reader = None
with open(single_file) as csv_file:
    area_titles_cvs_reader = csv.reader(csv_file, delimiter=',')
    line_count = 0
    for row in area_titles_cvs_reader:
        # Excludes csv colum titles
        if line_count == 0:
            line_count += 1
            continue
        # Assigns the fips code 
        fips = row[0]
        # Excludes fips 
        if row[0][2:] == "000" or fips[:2] == "US" or fips[:1] == "C":
            continue
        # Statistics over all industries 
        if row[2] == "10" and row[1] == "0":
            if fips not in fips_used:
                # Assigns used fips for counting later
                fips_used.append(fips)
                # Total annual wages
                rpt.all.total_pay = rpt.all.total_pay + int(row[10])
                if row[10] in distinct_annual_wages and row[10] in unique_annual_wages: # Remove any value thats not unique
                    unique_annual_wages.remove(row[10])
                elif row[10] not in distinct_annual_wages:                                     # Add distinct values
                    distinct_annual_wages.append(row[10])
                    unique_annual_wages.append(row[10])
                annual_wages.append((area_codes.get(row[0]),int(row[10])))
                # Count Establishments 
                rpt.all.total_estab = rpt.all.total_estab + int(row[8])
                if row[8] in distinct_estab and row[8] in unique_estab: # Remove any value thats not unique
                    unique_estab.remove(row[8])
                if row[8] not in distinct_estab:                                     # Add distinct values
                    distinct_estab.append(row[8])
                    unique_estab.append(row[8])
                area_estab.append((area_codes.get(row[0]),int(row[8])))
                # Count Employment levels
                rpt.all.total_empl = rpt.all.total_empl + int(row[9])
                if row[9] in distinct_empl and row[9] in unique_empl: # Remove any value thats not unique
                    unique_empl.remove(row[9])
                if row[9] not in distinct_empl:                                     # Add distinct values
                    distinct_empl.append(row[9])
                    unique_empl.append(row[9])
                average_employment.append((area_codes.get(row[0]),int(row[9])))
                # Record the area with the max annual wages 
                if high_total_annual_wages[1] < int(row[10]):
                    high_total_annual_wages[0] = int(fips)
                    high_total_annual_wages[1] = int(row[10]) 
                # Record the area with the max num of establisments
                if fips_estab[1] < int(row[8]):
                    fips_estab[0] = row[0]
                    fips_estab[1] = int(row[8]) 
                # Record the area with the max num of employment
                if fips_empl[1] < int(row[9]):
                    fips_empl[0] = row[0]
                    fips_empl[1]= int(row[9])
                
        # Statistics over the software publishing industry
        if row[2] == "5112" and row[1] == "5":
            if fips not in sp_fips_used:
                # Assigns used fips for counting later
                sp_fips_used.append(fips)
                # Total Pay
                rpt.soft.total_pay = rpt.soft.total_pay + int(row[10])
                if row[10] in sp_distinct_annual_wages and row[10] in sp_unique_annual_wages: # Remove any value thats not unique
                    sp_unique_annual_wages.remove(row[10])
                elif row[10] not in sp_distinct_annual_wages:                                        # Add distinct values
                    sp_distinct_annual_wages.append(row[10])
                    sp_unique_annual_wages.append(row[10])
                s_annual_wages.append((area_codes.get(row[0]),int(row[10])))
                # Count Establishments 
                rpt.soft.total_estab = rpt.soft.total_estab + int(row[8])
                if row[8] in sp_distinct_estab and row[8] in sp_unique_estab: # Remove any value thats not unique
                    sp_unique_estab.remove(row[8])
                if row[8] not in sp_distinct_estab:                                     # Add distinct values
                    sp_distinct_estab.append(row[8])
                    sp_unique_estab.append(row[8])
                s_area_estab.append((area_codes.get(row[0]),int(row[8])))
                # Count Employment levels
                rpt.soft.total_empl = rpt.soft.total_empl + int(row[9])
                if row[9] in sp_distinct_empl and row[9] in sp_unique_empl: # Remove any value thats not unique
                    sp_unique_empl.remove(row[9])
                if row[9] not in sp_distinct_empl:                                     # Add distinct values
                    sp_distinct_empl.append(row[9])
                    sp_unique_empl.append(row[9])
                s_average_employment.append((area_codes.get(row[0]),int(row[9])))
                # Record the area with the max annual wages
                if sp_high_total_annual_wages[1] < int(row[10]):
                    sp_high_total_annual_wages[0] = int(fips)
                    sp_high_total_annual_wages[1] = int(row[10]) 
                # Record the area with the max num of establisments
                if sp_fips_estab[1] < int(row[8]):
                    sp_fips_estab[0] = row[0]
                    sp_fips_estab[1] = int(row[8]) 
                # Record the area with the max num of employment
                if sp_fips_empl[1] < int(row[9]):
                    sp_fips_empl[0] = row[0]
                    sp_fips_empl[1]= int(row[9])


        line_count = line_count + 1


# Open File
with open(area_titles) as csv_file:
    area_titles_cvs_reader = csv.reader(csv_file, delimiter=',')
    line_count = 0

    for row in area_titles_cvs_reader:
        # Excludes csv colum titles
        if line_count == 0:
            line_count += 1
            continue
        # Replace fips area code with the fips area string
        if row[0] == str(high_total_annual_wages[0]):
            high_total_annual_wages[0] = row[1]
        if row[0] == str(fips_estab[0]):
            fips_estab[0] = row[1]
        if row[0] == str(fips_empl[0]):
            fips_empl[0] = row[1]
        if row[0] == str(sp_high_total_annual_wages[0]):
            sp_high_total_annual_wages[0] = row[1]
        if row[0] == str(sp_fips_estab[0]):
            sp_fips_estab[0] = row[1]
        if row[0] == str(sp_fips_empl[0]):
            sp_fips_empl[0] = row[1]
        



# Get top Five
annual_wages.sort(key = operator.itemgetter(1),reverse = True)
rpt.all.top_annual_wages = [annual_wages[0],annual_wages[1],annual_wages[2],annual_wages[3],annual_wages[4]]
area_estab.sort(key = operator.itemgetter(1),reverse = True)
rpt.all.top_annual_estab = [area_estab[0],area_estab[1],area_estab[2],area_estab[3],area_estab[4]]
average_employment.sort(key = operator.itemgetter(1),reverse = True)
rpt.all.top_annual_avg_emplvl =[average_employment[0],average_employment[1],average_employment[2],average_employment[3],average_employment[4]]
s_annual_wages.sort(key = operator.itemgetter(1),reverse = True)
rpt.soft.top_annual_wages = [s_annual_wages[0],s_annual_wages[1],s_annual_wages[2],s_annual_wages[3],s_annual_wages[4]]
s_area_estab.sort(key = operator.itemgetter(1),reverse = True)
rpt.soft.top_annual_estab = [s_area_estab[0],s_area_estab[1],s_area_estab[2],s_area_estab[3],s_area_estab[4]]
s_average_employment.sort(key = operator.itemgetter(1),reverse = True)
rpt.soft.top_annual_avg_emplvl =[s_average_employment[0],s_average_employment[1],s_average_employment[2],s_average_employment[3],s_average_employment[4]]

# Cache County's Rank for all industries    
for i in range(len(annual_wages)):
    wages = annual_wages[i]
    estab = area_estab[i]
    employment = average_employment[i]
    if wages[0] == "Cache County, Utah":
        rpt.all.cache_co_pay_rank = i + 1
    if estab[0] == "Cache County, Utah":
        rpt.all.cache_co_estab_rank = i + 1
    if employment[0] == "Cache County, Utah":
        rpt.all.cache_co_empl_rank = i + 1

# Cache County's Rank for software publishing 
for i in range(len(s_annual_wages)):
    wages = s_annual_wages[i]
    estab = s_area_estab[i]
    employment = s_average_employment[i]
    if wages[0] == "Cache County, Utah":
        rpt.soft.cache_co_pay_rank = i + 1
    if estab[0] == "Cache County, Utah":
        rpt.soft.cache_co_estab_rank = i + 1
    if employment[0] == "Cache County, Utah":
        rpt.soft.cache_co_empl_rank = i + 1

# Set rpt values
rpt.all.count = len(fips_used)
rpt.all.max_pay = high_total_annual_wages
rpt.all.unique_pay = len(unique_annual_wages)
rpt.all.distinct_pay = len(distinct_annual_wages)
rpt.all.per_capita_avg_wage = rpt.all.total_pay / rpt.all.total_empl
rpt.all.max_estab = fips_estab
rpt.all.unique_estab = len(unique_estab)
rpt.all.distinct_estab = len(distinct_estab)
rpt.all.max_empl = fips_empl
rpt.all.unique_empl = len(unique_empl)
rpt.all.distinct_empl = len(distinct_empl)
rpt.soft.count = len(sp_fips_used)
rpt.soft.max_pay = sp_high_total_annual_wages
rpt.soft.unique_pay = len(sp_unique_annual_wages)
rpt.soft.distinct_pay = len(sp_distinct_annual_wages)
rpt.soft.per_capita_avg_wage = rpt.soft.total_pay / rpt.soft.total_empl
rpt.soft.max_estab = sp_fips_estab
rpt.soft.unique_estab = len(sp_unique_estab)
rpt.soft.distinct_estab = len(sp_distinct_estab)
rpt.soft.max_empl = sp_fips_empl
rpt.soft.unique_empl = len(sp_unique_empl)
rpt.soft.distinct_empl = len(sp_distinct_empl)

# get time elapsed
end_time = time.time()
#print("\n\nTime took for program to run " + str(end_time - start_time))

# By the time you submit your work to me, this should be the *only* output your
# entire program produces.
print(rpt)
